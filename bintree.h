// TODO: modify the struct so it holds both the plaintext
// word and the hash.
typedef struct node {
    int data;
	char pass[50];
	char hash[33];
	struct node *left;
	struct node *right;
} node;

void insert(int key, node **leaf);

void print(node *leaf);

node *search(char hash, node *leaf);
